/*Разработайте функцию-конструктоp, которая будет создавать обьект Human(человек).
 Создайте массив обьектов и реализуйте функцию, которая будет сортировать элементы
  массива по значению свойств Age по возрастанию или по убыванию. */

  class Human {
    constructor(name, age, country) {
        this.name = name;
        this.age = age;
        this.country = country;
    }
}

  const user1 = new Human("Alexey", 35, "Ukraine");
  const user2 = new Human("John", 23, "USA");
  const user3 = new Human("Irina", 22, "Russia");
  const user4 = new Human("Irvin", 48, "Ireland");

  let people = [user1, user2, user3, user4];

  
  console.log(people.sort((a,b)=> a.age - b.age));  //сортировка по возрастанию Age
   
  
  console.log(people.sort((a,b)=> b.age - a.age));  //сортировка по убыванию Age

/*Разработайте функцию-конструктоp, которая будет создавать обьект Human(человек).
 Добавьте на свое усмотрение свойства и методы в этот обьект. Подумайте, какие методы 
 и свойства следует сделать уровня экземпляра а какие уровня функции - конструктора */

 class Human1 {
  constructor(name1, age1, country1) {
      this.name1 = name1;
      this.age1 = age1;
      this.country1 = country1;
  }
}

Human1.AdultCount = function() {      //статичная функция подсчета пользователей младше 18 лет
  let j = 0;
  people1.forEach(element => {
  if (element.age1 <18) {
    j ++;
  }
  });
  console.log(j);
}

const user5 = new Human1("Nick", 17, "Urugvai");
const user6 = new Human1("Bella", 30, "USA");
const user7 = new Human1("Olya", 16, "Ukraine");

let people1 = [user5, user6, user7];

Human1.AdultCount(people1);
