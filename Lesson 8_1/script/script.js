/*Нарисовать на странице круг используя параметры, которые введет пользователь.<br>
                При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна
                являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и
                добавлен на страницу с помощью Javascript<br>
                При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на
                кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный
                круг - этот круг должен
                исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.*/

window.addEventListener("load", () => {
    const circleContainer = document.createElement("div");
    circleContainer.classList.add("circleContainer");


    const input = document.createElement("input");  //поле ввода для диаметра круга
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "Введите диаметр круга");
    input.setAttribute("value", "");

    const submit = document.createElement("input");     // кнопка "нарисовать"
    submit.setAttribute("type", "submit");
    submit.setAttribute("value", "Нарисовать");

    const container = document.querySelector(".container");
    const startButton = document.querySelector(".button");

    startButton.addEventListener("click", () => { //при нажатии на кнопку "нарисовать круг" добавляем поле ввода 
        startButton.remove();                     // диаметра круга и кнопку "нарисовать" в тело страницы
        container.append(input);
        container.append(submit);
    });

    submit.addEventListener("click", () => {    //удаляем поле ввода и кнопку после клика по кнопке "нарисовать"
        const diametr = input.value + "px";
        input.remove();
        submit.remove();
        const getColor = () => {    //случайный цвет кругов
            let r = Math.floor(Math.random() * (256)),
                g = Math.floor(Math.random() * (256)),
                b = Math.floor(Math.random() * (256));
            return '#' + r.toString(16) + g.toString(16) + b.toString(16);
        }
        let i = 0;              //рисуем 100 кругов заданного юзером диаметра и случайного цвета
        while (i < 100) {
            const circle = document.createElement("div");
            circle.setAttribute("class", "circle");
            circle.style.width = diametr;
            circle.style.height = diametr;
            circle.style.backgroundColor = getColor();
            circleContainer.append(circle);
            i++;
        }
    });
    container.append(circleContainer);          //удаляем круги при клике на них
    circleContainer.addEventListener("click", (e) => {
        e.target.remove();
    });
});
