


/*Задание 1:
Создайте на странице div и дайте ему внешний отступ 150 пикселей. Используя JS выведите в консоль отступ
*/
const block = document.createElement("div");
block.setAttribute("style", "margin : 150px");
document.body.append(block);
let style = getComputedStyle(block);
console.log(`Внешние отступы блока: ${style.margin}`)




/*Задание 2:
Создайте приложение секундомер.
Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый * Вывод счётчиков в формате ЧЧ:ММ:СС
Реализуйте Задание используя синтаксис ES6 и стрелочные функции
*/

window.addEventListener("load", () => {

    let timerField = document.querySelector(".timer");

    const secundomer = {

        // Инициализация переменных
        seconds: 0,
        minutes: 0,
        hours: 0,
        startTimer: null,

        start: function () {
            				// Функция старта секундомера
            clearInterval(secundomer.startTimer);
            secundomer.startTimer = setInterval(secundomer.runSecundomer, 1000);
            timerField.style.backgroundColor = "green";

        },

        stop: function () {				// Функция остановки секундомера
            timerField.style.backgroundColor = "red";
            clearInterval(secundomer.startTimer);

        },

        reset: function () {				// Функция сброса секундомера
            timerField.style.backgroundColor = "grey";
            clearInterval(secundomer.startTimer);
            secundomer.seconds = 00;
            secundomer.minutes = 0;
            secundomer.hours = 0;
            secundomer.startTimer = null;

            secundomer.showTime();
        },

        runSecundomer: function () {				 // Функция подсчета минут и часов
            secundomer.showTime();
            if (secundomer.seconds == 60) {
                secundomer.minutes++;
                secundomer.seconds = 0;
            }
            if (secundomer.minutes == 60) {
                secundomer.hours++;
                secundomer.minutes = 0;
            }
            secundomer.seconds++;
        },

        showTime: function () {
            timerField.value = `${secundomer.hours}:${secundomer.minutes}:${secundomer.seconds}`;
        },
    };

    // Отображаем секундомер
    secundomer.showTime();

    // обработчики для кнопок
    document.querySelector('.start').onclick = secundomer.start;
    document.querySelector('.stop').onclick = secundomer.stop;
    document.querySelector('.reset').onclick = secundomer.reset;

})



/*
Задание 3:
Реализуйте программу проверки телефона
Используя JS Создайте поле для ввода телефона и кнопку сохранения
Пользователь должен ввести номер телефона в формате 000-000-00-00
Поле того как пользователь нажимает кнопку сохранить проверте правильность ввода номера, 
если номер правильный сделайте зеленый фон и используя document.location 
переведите пользователя на страницу https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg 
если будет ошибка отобразите её в диве до input.
*/
const checkNumber = document.querySelector(".checkNumber"); //находим DIV с инпутами и создаем поле ввода номера
const inputField = document.createElement("input");
inputField.setAttribute("placeholder", "000-000-00-00");
checkNumber.append(inputField);

const button = document.createElement("input"); //создаем кнопку отправки номера
button.setAttribute("type", "submit");
button.setAttribute("value", "Сохранить");
checkNumber.append(button);

const pattern = /^\d\d\d-\d\d\d-\d\d-\d\d$/;    //создаю регулярку

button.onclick =  () => {       

    if (pattern.exec(inputField.value)) {           //если номер соответствует шаблону
        inputField.style.backgroundColor = "green";
        document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg"
    }else{
      const errorText = document.createElement("p");    //если не соответствует
      errorText.innerHTML = "Вы ошиблись. Обновите страницу и попробуйте еще раз";
      checkNumber.prepend(errorText);
    }


}



                 /*Задание 4:

Создайте слайдер который каждые 3 сек будет менять изображения
Изображения для отображения
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
*/
const images = document.querySelector(".images");
const image = document.querySelector("img");
const imagesContainer = [       //массив картинок
"https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg", 
"https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg", 
"https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
"https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
"https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg"
];


let i = 0;  //функция смены источника изображения в div*е
window.setInterval(function changeImage () {
    image.src = imagesContainer[i];
    i++;

    if (i== imagesContainer.length) {   //после показа последней картинки - обнулить счетчик
        i = 0;
    }
}, 3000); 







