//Создать объект “Документ” с свойствами “Заголовок, тело, футер, дата”.
//Создать вложенный объект - “Приложение”.Создать в объекте “Приложение”, 
//вложенные объекты, “Заголовок, тело, футер, дата”.Создать методы для
//заполнения и отображения документа


const document1 = {
    title: "t",
    body: "b",
    footer: "f",
    data: "d",
    app : {     //вложенный обьект - "приложение" с вложенным обьектом "заголовок"
        title: {                        
            secondTitle : "second",
            thirdTitle : "third",
        },
        body: {},
        footer: {},
        date: {},
    },
    add: function() {           // метод для ввода данных в обьект
        this.title = prompt("Введите заголовок документа");
       
        this.body = prompt("Введите основной текст документа");
        this.footer = prompt("Введите футер документа");
        this.date = prompt("Введите дату документа");

        this.app.title.firstTitle = prompt("Введите название для приложения");
        this.app.title.secondTitle = prompt("Введите альтернативное название для приложения");

        this.app.body = prompt("Введите основной текст для приложения");
        this.app.footer = prompt("Введите футер приложения");
        this.app.date = prompt("Введите дату для приложения");
        
    },

    output: function() {        // метод для вывода данных на экран 
        document.write("Название документа: " + this.title + "<br>");
        document.write("Основной текст документа: " + this.body + "<br>");
        document.write("Подвал документа: " + this.footer + "<br>");
        document.write("Дата создания документа: " + this.date + "<br>");

        document.write("Рабочее название приложения: " + this.app.title.firstTitle + "<br>");
        document.write("Альтернативное название приложения: " + this.app.title.secondTitle + "<br>");

        document.write("Суть приложения: " + this.app.body + "<br>");
        document.write("Подвал приложения: " + this.app.footer + "<br>");
        document.write("Дата релиза приложения: " + this.app.date + "<br>");
        
    }

}
document1.add();            //запуск методов
document1.output();
